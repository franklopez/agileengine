import styled, { css } from "styled-components";
import { HiOutlineChevronLeft, HiOutlineChevronRight } from "react-icons/hi";

import colors from "../../styles/colors";

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  width: 100%;
  min-height: 36px;
  
  color: ${colors.primary};

  div {
    display: flex;
    align-items: center;
  }

  select {
    border: none;
    background: transparent;
    color: ${colors.primary};
  }
`;

export const PreviousPage = styled(HiOutlineChevronLeft)`
  margin-right: 10px;
  cursor: pointer;
  ${(props) =>
    props.disabled &&
    css`
      color: ${colors.secondary};
      cursor: default;
    `}
`;

export const NextPage = styled(HiOutlineChevronRight)`
  margin-right: 10px;
  cursor: pointer;
  ${(props) =>
    props.disabled &&
    css`
      color: ${colors.secondary};
      cursor: default;
    `}
`;
