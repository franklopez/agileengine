import styled from "styled-components";
import colors from "../../styles/colors";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 500px;
  position: relative;

  > svg {
    fill: ${colors.secondary};
    position: absolute;
    top: 18px;
    right: 20px;
  }
 `;

export const Input = styled.input`
  height: 50px;
  line-height: 25px;
  outline: none;

  flex: 1;
  background: ${colors.white};
  border-radius: 5px;

  border: 1px solid
    ${(props) => (props.focus ? colors.secondary : colors.lightgray)};

  padding: 0 40px 0 20px;
  font-size: 16px;
  color: ${colors.primary};

  &::placeholder {
    font-size: 16px;
    color: ${colors.secondary};
  }
`;
