import React, { useState } from "react";
import { FaSearch } from "react-icons/fa";

import { Container, Input } from "./styles";

const Search = ({ setText, ...rest }) => {
  const [focus, setFocus] = useState(false);

  const handleInputChange = (e) => {
    setText(e.target.value);
  }

  return (
    <Container focus={focus} >
      <Input
        placeholder="Search"
        onChange={handleInputChange}
        onMouseEnter={() => setFocus(true)}
        onMouseLeave={() => setFocus(false)}
        focus={focus}
        {...rest}
      />
      <FaSearch />
    </Container>
  );
};

export default Search;
