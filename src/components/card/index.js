import React from "react";
import {Container, Text, Type} from "./styles";
import NoImage from '../../resources/no-image.png';

const Card = ({item}) => {
    return(
        <Container >
            <img alt="logo" src={item?.image_front_thumb_url || NoImage} area="image" />
            <Text area="name">
                <span>{item?.product_name}</span>
                <span>{item?.origins}</span>
            </Text>
            <Type area="type">
                {item.compared_to_category.split(':')[1].split('-')[0]}
            </Type>
            <Text area="weight">
                <span>{item?.quantity?.toLowerCase().replace(/\s/g, "")}</span>
                <span>{item?.entry_dates_tags[2]}</span>
            </Text>
        </Container>
    )
}

export default Card;