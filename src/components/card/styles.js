import styled from "styled-components";
import colors from "../../styles/colors";

export const Text = styled.div`
  display: flex;
  flex-direction: column;
  grid-area: ${props => props.area ? props.area : ''};
  color:${colors.primary};

  span:first-of-type {
    font-weight: bold;
  }

  span:last-of-type {
    font-size: 13px;
    color: ${colors.secondary};
  }
  
`;

export const Type = styled.div`
  grid-area: ${props => props.area ? props.area : ''};
  background-color: ${colors.secondary};
  display: flex;
  justify-content: center;
  align-items: center;
  max-width: 200px;
  height: 40px;
  border-radius: 5px;
  color: ${colors.white};
  text-transform: uppercase;
`;

export const Container = styled.div`
  display: grid;
  grid-template-columns: 10% 40% 35% 15%;
  grid-template-areas: 'image name type weight';

  align-items: center;
  column-gap: 20px;
  padding: 10px;
  margin: 0 10px;
  row-gap: 10px;
  border-radius: 5px;
  border: solid 1px ${colors.secondary};
  background-color: ${colors.white};

  > img {
    width: auto;
    height: 40px;
    border-radius: 5px;
  }

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr minmax(30%,auto);
    grid-template-areas: 'name image' 'type weight';
    column-gap: 10px;
  }
`;