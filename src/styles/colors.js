const colors = {
    primary: "#7B3F00",
    secondary: "#D27D2D",
    white: "#ffffff",
    lightgray: "#EDEDED",
  };
  
  export default colors;