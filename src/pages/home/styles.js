import styled from "styled-components";
import colors from "../../styles/colors";

export const Header = styled.div`
    width: 100%;    
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    column-gap: 20px;
    
    img {
        width: auto;
        height: 150px;
    }
`;

export const Title = styled.span`
    font-size: 65px;    
    font-weight: 600;
    color: ${colors.primary};
`;

export const TableContainer = styled.div`
  width: 100%;
  display: flex;
  
  height: calc(100vh - 150px);
  display: flex;
  flex-direction: column;
  row-gap: 10px;
  padding: 10px 0;

  overflow-y: auto;
  overflow-x: hidden;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: column;

    width: 100%;
    height: 100vh;

    @media screen and (max-width: 768px) {

        ${Title} {
            display: none;
        }

        ${TableContainer} {
           height: 100vh;
   
        }

        ${Header}{
            margin-top: 10px;

            img {
                width: 50px;
                height: auto;
            }
        }
        
    }
`;