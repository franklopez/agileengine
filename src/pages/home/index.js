import React, { useState, useEffect } from "react";
import { useTable, usePagination } from "react-table";

import { Container, Header, Title, TableContainer } from './styles';
import Logo from '../../resources/logo.jpg';
import Pagination from "../../components/pagination";
import Card from "../../components/card";
import Search from "../../components/search";

const columns = [];

const Home = () => {
    const [items,setItems] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [search, setSearch] = useState('');

    const {
        state: { pageIndex, pageSize },
        previousPage,
        nextPage,
        setPageSize,
        canPreviousPage,
        canNextPage,
      } = useTable(
        {
          columns,
          data: items,
          initialState: { pageIndex: 0 },
          manualPagination: true,
          pageCount,
        },
        usePagination
      );

      const loadItems = async (pageIndex, pageSize, search, country) => {
        const response = await fetch(
          `https://world.openfoodfacts.org/category/chocolates.json?page=${
            pageIndex + 1
          }&page_size=${pageSize}`,
          {
            method: "GET",
          }
        ).then(async (response) => {
          return response.json();
        });
    
        setPageCount(response.page_count);
        let itemsLst = response.products;
    
        if (search?.length > 0) {
          itemsLst = itemsLst.filter(item =>
            item.product_name?.toLowerCase().includes(search)
            || (item.brands && item.brands?.toLowerCase().includes(search))
            || item._keywords?.includes(search)
          )
        }
  
        console.log(items);
        setItems(itemsLst);
      };
    
      useEffect(() => {
        loadItems(pageIndex, pageSize, search);
      }, [pageIndex, pageSize, search]);

      return(
        <Container>
            <Header>
                <img src={Logo} />
                <Title>Chocolats</Title>
                <Search setText={setSearch}/>
            </Header>
            <TableContainer>
                {items.map((item) => (
                <Card key={item.code} item={item} />
            ))}
            </TableContainer>
            <Pagination
            previousPage={previousPage}
            nextPage={nextPage}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            pageIndex={pageIndex}
            pageCount={pageCount}
            pageSize={pageSize}
            setPageSize={setPageSize}
            />
        </Container>
      );
};

export default Home;